# kde4-fedora

usefull git aliases

    alias rpmbuild='rpmbuild -bs'    
    alias obsoletes='rpmspec -q --obsoletes'    
    alias provides='rpmspec -q --provides'   
    alias requires='rpmspec -q --requires'  
    alias add='git add'  
    alias commit='git commit'  
    alias push='git push'  
    alias status='git status'  

Building SRPMS:
rpmbuild -bs SPECS/kde-workspace.spec --define "_sourcedir SOURCES/kde-workspace-4.11.22-5.fc23.src/" --define '_srcrpmdir SRPMS'
rpmbuild -bs SPECS/colord-kde.spec --define "_sourcedir SOURCES/colord-kde-0.3.0-7.fc22.src/" --define '_srcrpmdir SRPMS'
rpmbuild -bs SPECS/kde-print-manager.spec --define "_sourcedir SOURCES/kde-print-manager-4.14.3-3.fc21.src/" --define '_srcrpmdir SRPMS'
rpmbuild -bs SPECS/kdeplasma-addons.spec --define "_sourcedir SOURCES/kdeplasma-addons-4.14.3-3.fc22.src/" --define '_srcrpmdir SRPMS'
rpmbuild -bs SPECS/kde-baseapps.spec --define "_sourcedir SOURCES/kde-baseapps-15.04.3-2.fc22.1.src//" --define '_srcrpmdir SRPMS'
rpmbuild -bs SPECS/kactivities.spec  --define "_sourcedir SOURCES/kactivities-4.13.3-10.fc23.src/" --define '_srcrpmdir SRPMS'
rpmbuild -bs SPECS/kscreen.spec  --define "_sourcedir SOURCES/kscreen-1.0.2.1-6.fc21.src" --define '_srcrpmdir SRPMS'
rpmbuild -bs SPECS/dolphin-plugins.spec --define "_sourcedir SOURCES/dolphin-plugins-15.04.3" --define '_srcrpmdir SRPMS'


Add a package to the bitbucket:
wget https://kojipkgs.fedoraproject.org//packages/kactivities/4.13.3/10.fc23/src/kactivities-4.13.3-10.fc23.src.rpm
ark -ba kactivities-4.13.3-10.fc23.src.rpm
mv kactivities-4.13.3-10.fc23.src/kactivities.spec ../SPECS/




