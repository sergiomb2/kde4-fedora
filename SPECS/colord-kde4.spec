%define original_name colord-kde

Name:           colord-kde4
Version:        0.3.0
Release:        12%{?dist}
Summary:        Colord support for KDE4

License:        GPLv2+
URL:            https://projects.kde.org/projects/playground/graphics/colord-kde
Source0:        http://download.kde.org/stable/colord-kde/%{version}/src/%{original_name}-%{version}.tar.bz2
Patch0:		colord-kde4-rename.patch
Patch1:		colord-kde4-f24-cmake.patch

# upstreamable patch - X-KDE-ParentApp has to be set to kcontrol to be visible

BuildRequires:  cmake kdelibs4-devel colord-devel

# need kcmshell4 from kde-runtime at least (from kcm_touchpad SPEC)
Requires: kde-runtime%{?_kde4_version: >= %{_kde4_version}}

# colord is a dbus daemon
Requires: colord
# for kcmshell
%{?kde4_runtime_requires}


%description
KDE support for colord including KDE Daemon module and System Settings module.


%prep
%setup -q -n colord-kde-%{version}
%patch0
%if (0%{?fedora} && 0%{?fedora} > 22)
%patch1
%endif

%build
mkdir %{_target_platform}
pushd %{_target_platform}
%{cmake_kde4} ..
popd

make %{?_smp_mflags} -C %{_target_platform}


%install
make install/fast DESTDIR=%{buildroot} -C %{_target_platform}


%files
%doc COPYING MAINTAINERS TODO
%{_kde4_libdir}/kde4/*.so
%{_kde4_sharedir}/kde4/services/kded/colord.desktop
%{_kde4_sharedir}/kde4/services/kcm_colord.desktop
%{_kde4_bindir}/colord-kde4-icc-importer
%{_kde4_datadir}/applications/kde4/colordkdeiccimporter.desktop


%changelog
* Sun Oct 23 2016 Sérgio Basto <sergio@serjux.com> - 0.3.0-12
- Do not Obsolete/provides colord-kde

* Mon Jul 04 2016 Piotr Gbyliczek <p.gbyliczek@node4.co.uk> 0.3.0-11
- added original package name to source0 via variable
- Patch for f24 cmake errors (#29)

* Sat Nov 14 2015 Piotr Gbyliczek <p.gbyliczek@node4.co.uk> 0.3.0-10
- Merged changes from Sergio's fork

* Wed Nov 11 2015 Rex Dieter <rdieter@fedoraproject.org> 0.3.0-9
- Spec changes for smoother build and install experience.

* Wed Nov 11 2015 Piotr Gbyliczek <p.gbyliczek@node4.co.uk> 0.3.0-9
- Added colord-kde4-rename.patch to allow install side by side with never colord-kde versions

* Mon Nov 09 2015 Piotr Gbyliczek <p.gbyliczek@node4.co.uk> 0.3.0-8
- Initial stable release for kde4-fedora repo
- Renamed colord-kde to colord-kde4

* Sat May 02 2015 Kalev Lember <kalevlember@gmail.com> - 0.3.0-6
- Rebuilt for GCC 5 C++11 ABI change

* Fri Mar 06 2015 Rex Dieter <rdieter@fedoraproject.org> 0.3.0-5
- update URL:, +%%{?kde4_runtime_requires}

* Sat Aug 16 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.3.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Mon May 27 2013 Lukáš Tinkl <ltinkl@redhat.com> - 0.3.0-1
- New upstream version 0.3.0

* Wed Feb 13 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Wed Jul 18 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Thu Apr 05 2012 Jaroslav Reznik <jreznik@redhat.com> - 0.2.0-1
- update to version 0.2.0

* Thu Mar 22 2012 Jaroslav Reznik <jreznik@redhat.com> - 0.1.0-2
- fix kcmshell4 visibility by setting X-KDE-ParentApp

* Wed Mar 21 2012 Jaroslav Reznik <jreznik@redhat.com> - 0.1.0-1
- initial try
